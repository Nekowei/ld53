using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputManager), typeof(TerrainManager), typeof(UIManager))]
public class GameManager : MonoBehaviour
{

    public static int score;
    public static int speedLevel;
    public static int rotationLevel;
    public static int secret;

    void Awake()
    {
        C.Init();
        score = 0;
        speedLevel = 1;
        rotationLevel = 1;
        secret = 0;
    }

}
