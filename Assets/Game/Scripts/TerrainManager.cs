using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour
{

    public void Init(GameObject waterPrefab)
    {
        for (int i = -2; i < 2; i++)
        {
            for(int j = -2; j < 2; j++)
            {
                var g = Instantiate(waterPrefab);
                g.transform.SetParent(transform, false);
                g.transform.position = new Vector3(i * 100, 0, j * 100);
            }
        }
    }

}
