using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class VolcanoController : MonoBehaviour
{
    public ParticleSystem ps;
    public ParticleSystem ps2;
    public GameObject water;
    public float countdown;
    public float seaLevel;
    public Transform sunLight;
    public Color victoryColor;

    private bool countdownOver;

    private void Start()
    {
        ps.Play();
        ps2.Pause();
        C.ec.gameOver += OnGameOver;
        countdownOver = false;
    }

    private void Update()
    {
        if (Time.realtimeSinceStartup > countdown && !countdownOver)
        {
            countdownOver = true;
            C.ec.countdownOver.Invoke();
            StartCoroutine(Raising());
            ps.Stop();
            ps2.Play();
            for (int i = 0; i < water.transform.childCount; i++)
            {
                var w = water.transform.GetChild(i);
                DOTween.To(() => w.GetComponent<Renderer>().material.color, 
                    c => w.GetComponent<Renderer>().material.color = c,
                    Color.red, 20);
            }
            sunLight.DORotate(new Vector3(0, -222, 0), 20f);
        }
    }

    private IEnumerator Raising()
    {
        while (water.transform.position.y < seaLevel)
        {
            yield return new WaitForSeconds(0.1f);
            var tp = water.transform.position;
            water.transform.position = new Vector3(tp.x, tp.y + 0.2f, tp.z);

            C.ec.raisingUp.Invoke(water.transform.position.y);
        }
    }

    private void OnGameOver(bool victory)
    {
        if (victory)
        {
            ps2.Stop();
            for (int i = 0; i < water.transform.childCount; i++)
            {
                var w = water.transform.GetChild(i);
                DOTween.To(() => w.GetComponent<Renderer>().material.color,
                        c => w.GetComponent<Renderer>().material.color = c,
                        victoryColor, 10);
            }
            sunLight.DORotate(new Vector3(45, -222, 0), 10f);
        }
        else
        {
            sunLight.DORotate(new Vector3(-15, -222, 0), 10f, RotateMode.FastBeyond360);
        }
    }
}
