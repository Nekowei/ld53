using Nekowei.YeahUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoController : MonoBehaviour
{
    public Owner owner;
    public float mass;
    public PhysicMaterial material;

    private void Start()
    {
        GetComponent<Renderer>().material.color = SOLoader.Get<OwnerColor>().GetColor(owner);
    }

}

[Serializable]
public enum Owner
{
    red, blue, green, yellow, purple, white
}