using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OwnerColor", menuName = "Nekowei/OwnerColor")]
public class OwnerColor : ScriptableObject
{
    public OwnerColorPair[] pair;

    public Color GetColor(Owner owner)
    {
        foreach (var c in pair)
        {
            if (c.owner.Equals(owner))
            {
                return c.color;
            }
        }
        return Color.white;
    }

    public Color GetSelectedColor(Owner owner)
    {
        foreach (var c in pair)
        {
            if (c.owner.Equals(owner))
            {
                return c.selectedColor;
            }
        }
        return Color.black;
    }
}

[Serializable]
public class OwnerColorPair
{
    public Owner owner;
    public Color color;
    public Color selectedColor;
}