using DG.Tweening;
using Nekowei.YeahUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.Universal.Internal;

public class ShipController : MonoBehaviour
{

    public GameObject model;
    public Transform cargoSlot;
    public Transform target;

    public float speedForce = 1000;
    public float rotateForce = 1000;
    public float maxSpeed = 10;
    public float rotateSpeed = 100;
    [Range(0f, 1f)]
    public float speedDirLerp = 0.6f;

    public float dockingCameraDistance = 15;
    public float sailAwayDistance = 10;

    public TMP_Text pointUpPrefab;
    public float pointUpSpeed = 1;

    public ParticleSystem ps;
    public float psSpeedScale = 10;

    public AudioClip open;
    public AudioClip close;
    public AudioClip stradingSong;
    public AudioClip doomSong;
    public AudioClip hit;
    public AudioClip secret;

    public float hitCD = 5;
    private float lastHitTime = 0;

    private float _maxSpeed;
    private float _rotateSpeed;

    private CameraController cc;

    private bool isDocking = false;

    private GameObject[] cargoList = new GameObject[6];
    private List<GameObject> lostCargo = new();

    public void Start()
    {
        _maxSpeed = maxSpeed;
        _rotateSpeed = rotateSpeed;

        cc = gameObject.AddComponent<CameraController>();
        C.ec.zoom += cc.OnCameraZoom;
        C.ec.pointerMove += cc.OnCameraRotate;
        cc.Follow(target);

        C.ec.move += OnMove;
        C.ec.unload += OnUnload;
        C.ec.docking += OnDocking;
        C.ec.sailAway += OnSailAway;
        C.ec.addCargo += OnAddCargo;
        C.ec.upgradeSpeed += OnSpeedUpgrade;
        C.ec.upgradeRotation += OnRotationUpgrade;
        C.ec.countdownOver += OnCountdownOver;
        C.ec.raisingUp += OnRaisingUp;
        C.ec.gameOver += OnGameOver;

        var a = GetComponent<AudioSource>();
        a.clip = stradingSong;
        a.Play();
        a.loop = true;
    }

    private void Update()
    {
        if (!isDocking)
        {
            var tr = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(new Vector3(0, tr.y, 0));
        }
    }

    public void OnMove(Vector2 axis)
    {
        if (!isDocking)
        {
            var rb = GetComponent<Rigidbody>();

            rb.AddForce(-axis.y * speedForce * Time.deltaTime * transform.forward);
            var speed = rb.velocity.magnitude;
            var dir = Vector3.Dot(rb.velocity, transform.forward) > 0 ? 1 : -1;
            rb.velocity = Vector3.Lerp(rb.velocity, dir * speed * transform.forward, speedDirLerp);
            if (rb.velocity.magnitude > _maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * _maxSpeed;
            }

            var e = rb.transform.rotation.eulerAngles;
            var a = axis.x * rotateForce * Time.deltaTime;
            if (Mathf.Abs(a) > _rotateSpeed)
            {
                a = a > 0 ? _rotateSpeed : -_rotateSpeed;
            }
            rb.transform.rotation = Quaternion.Euler(
                e.x, e.y + a, e.z);

            var m = ps.main;
            m.startSpeed = rb.velocity.magnitude / psSpeedScale;
        }
    }

    public void OnUnload(Owner owner)
    {
        isDocking = true;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        var t = transform.position + Vector3.up * dockingCameraDistance;
        Camera.main.transform.DOMove(t, 0.5f)
        .OnUpdate(() => {
            if (Camera.main.transform.rotation.eulerAngles.x < 89)
            {
                Camera.main.transform.LookAt(transform);
            }
        });
        cc.Follow(null);
        StartCoroutine(Unload(owner));
    }

    private IEnumerator Unload(Owner owner)
    {
        for (int i = 0; i < cargoList.Length; i++) 
        {
            if (cargoList[i] != null 
                && cargoList[i].GetComponent<CargoController>().owner == owner)
            {
                yield return new WaitForSeconds(0.3f);
                Debug.Log($"unload cargo at {i}");
                Destroy(cargoList[i]);
                cargoList[i] = null;

                var g = Instantiate(pointUpPrefab);
                g.transform.position = transform.position - Camera.main.transform.forward * 3;
                g.transform.rotation = Camera.main.transform.rotation;
                var target = g.transform.position + Camera.main.transform.up * 3;
                g.transform.DOMove(target, 0.5f).OnComplete(() => {
                    Destroy(g);
                    C.ec.cargoAdded.Invoke();
                });
            }
        }
        for (int i = 0; i < lostCargo.Count; i++)
        {
            if (lostCargo[i] != null 
                && Vector3.Distance(lostCargo[i].transform.position, transform.position) < 5
                && lostCargo[i].GetComponent<CargoController>().owner == owner)
            {
                yield return new WaitForSeconds(0.3f);
                Debug.Log($"unload cargo at {i}");
                Destroy(lostCargo[i]);
                lostCargo[i] = null;

            }
        }
    }

    private void OnDocking()
    {
        var a = cargoSlot.GetComponent<AudioSource>();
        a.clip = open;
        a.Play();
    }

    private void OnSailAway()
    {
        var a = cargoSlot.GetComponent<AudioSource>();
        a.clip = close;
        a.Play();
        isDocking = false;
        var t = transform.position + (transform.up + transform.forward) * sailAwayDistance;
        foreach (var cargo in cargoList)
        {
            if (cargo != null)
            {
                var c = SOLoader.Get<OwnerColor>()
                    .GetColor(cargo.GetComponent<CargoController>().owner);
                cargo.GetComponent<Renderer>().material.color = c;
            }
        }
        Camera.main.transform.DOMove(t, 0.5f)
        .OnUpdate(() => {
            if (Camera.main.transform.rotation.eulerAngles.x > 10)
            {
                Camera.main.transform.LookAt(transform);
            }
        })
        .OnComplete(() => { cc.Follow(target); });
    }

    private void OnAddCargo(GameObject cargo)
    {
        for (int i = 0; i < cargoList.Length; i++)
        {
            if (cargoList[i] == null)
            {
                Debug.Log($"add cargo at {i}");
                Destroy(cargo.GetComponent<Rigidbody>());
                cargo.transform.SetParent(cargoSlot);
                int x = i % 2 == 0 ? 0 : -1;
                cargo.transform.localPosition = new Vector3(x, 0, i / 2);
                cargo.transform.rotation = new Quaternion();
                var cc = cargo.GetComponent<CargoController>();
                var c = SOLoader.Get<OwnerColor>().GetColor(cc.owner);
                cargo.GetComponent<Renderer>().material.color = c;
                cargoList[i] = cargo;
                break;
            }
        }
    }

    private void OnSpeedUpgrade(int level)
    {
        _maxSpeed = maxSpeed + level * 4;
    }

    private void OnRotationUpgrade(int level)
    {
        _rotateSpeed = rotateSpeed + level * 0.5f;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Finish"))
        {
            if (lastHitTime + hitCD < Time.realtimeSinceStartup)
            {
                lastHitTime = Time.realtimeSinceStartup;
                for (int i = 0; i < cargoList.Length; i++)
                {
                    if (cargoList[i] != null)
                    {
                        var p = UnityEngine.Random.Range(0, 6);
                        if (p == 0)
                        {
                            Debug.Log($"{cargoList[i]} unstable");
                            var rb = cargoList[i].AddComponent<Rigidbody>();
                            rb.mass = 0.01f;
                            cargoList[i].transform.SetParent(null);
                            lostCargo.Add(cargoList[i]);
                            cargoList[i] = null;
                            var a = cargoSlot.GetComponent<AudioSource>();
                            a.loop = false;
                            a.clip = hit;
                            a.Play();
                        }
                    }
                }
            }
        }
        else if (collision.collider.CompareTag("Secret"))
        {
            GameManager.secret++;
            Destroy(collision.collider.gameObject);
            var a = cargoSlot.GetComponent<AudioSource>();
            a.clip = secret;
            a.Play();
            a.loop = false;
        }
    }

    private void OnCountdownOver()
    {
        var a = GetComponent<AudioSource>();
        a.clip = doomSong;
        a.Play();
        a.loop = false;
    }

    private void OnRaisingUp(float value)
    {
        var tp = transform.position;
        transform.position = new Vector3(tp.x, value, tp.z);
    }

    private void OnGameOver(bool victory)
    {
        if (victory)
        {
            var a = GetComponent<AudioSource>();
            a.clip = stradingSong;
            a.Play();
            a.loop = true;
        }
    }

}
