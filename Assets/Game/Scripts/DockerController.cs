using DG.Tweening;
using Nekowei.YeahUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;

[RequireComponent(typeof(Collider))]
public class DockerController : MonoBehaviour
{
    public Transform docker;
    public GameObject[] cargoList;
    public Owner owner;

    private bool isEnter = false;
    private bool isDocking = false;
    private GameObject currentCargo = null;

    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    private void Start()
    {
        C.ec.docking += OnDocking;
        C.ec.pointerUp += OnPointerUp;
        C.ec.sailAway += OnSailAway;
    }

    private void Update()
    {
        if (isDocking)
        {
            foreach (var cargo in cargoList)
            {
                if (cargo != null && !cargo.Equals(currentCargo))
                {
                    var c = SOLoader.Get<OwnerColor>()
                        .GetColor(cargo.GetComponent<CargoController>().owner);
                    cargo.GetComponent<Renderer>().material.color = c;
                }
            }
            UpdateCurrentCargo();
        }
    }

    private void UpdateCurrentCargo()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit))
        {
            if (hit.collider.CompareTag("Respawn"))
            {
                currentCargo = hit.collider.gameObject;
                var c = SOLoader.Get<OwnerColor>()
                    .GetSelectedColor(currentCargo.GetComponent<CargoController>().owner);
                currentCargo.GetComponent<Renderer>().material.color = c;
            }
        }
    }

    private void OnPointerUp(Vector2 _)
    {
        if (isDocking)
        {
            UpdateCurrentCargo();
            if (currentCargo != null)
            {
                Debug.Log($"addCargo {owner}");
                C.ec.addCargo.Invoke(currentCargo);
                currentCargo = null;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!isDocking && other.CompareTag("Player"))
        {
            Debug.Log($"stay {owner}");
            isEnter = true;
            C.ec.showDockingUI.Invoke(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isDocking && other.CompareTag("Player"))
        {
            Debug.Log($"enter {owner}");
            isEnter = true;
            C.ec.showDockingUI.Invoke(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!isDocking && other.CompareTag("Player"))
        {
            Debug.Log($"exit {owner}");
            isEnter = false;
            C.ec.showDockingUI.Invoke(false);
        }
    }

    private void OnDocking()
    {
        if (isEnter)
        {
            Debug.Log($"docking {owner}");
            isDocking = true;
            C.ec.unload.Invoke(owner);
        }
    }

    private void OnSailAway()
    {
        Debug.Log($"sailAway {owner}");
        isDocking = false;
        foreach (var cargo in cargoList)
        {
            if (cargo != null)
            {
                var c = SOLoader.Get<OwnerColor>()
                    .GetColor(cargo.GetComponent<CargoController>().owner);
                cargo.GetComponent<Renderer>().material.color = c;
            }
        }
    }

}
