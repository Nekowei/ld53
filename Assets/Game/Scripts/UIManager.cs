using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Button docking;
    public Button sailAway;
    public GameObject infoPanel;
    public Button preButton;
    public Button nextButton;
    public Button exit;
    public Button tutorial;

    public Sprite[] tutorialImages;

    public Button upgrade;
    public GameObject upgradePanel;
    public TMP_Text upgradePoint;
    public TMP_Text speedLevel;
    public TMP_Text speedCost;
    public Button speedButton;
    public TMP_Text rotationLevel;
    public TMP_Text rotationCost;
    public Button rotationButton;

    public Transform victoryPanel;
    public Transform failedPanel;

    private int currentTutorial;

    private void Start()
    {
        docking.onClick.AddListener(OnDocking);
        sailAway.onClick.AddListener(OnSailAway);
        docking.gameObject.SetActive(false);
        sailAway.gameObject.SetActive(false);

        infoPanel.gameObject.SetActive(true);
        preButton.gameObject.SetActive(true);
        nextButton.gameObject.SetActive(true);
        currentTutorial = 0;
        tutorial.onClick.AddListener(OnToggleInfo);
        preButton.onClick.AddListener(OnPre);
        nextButton.onClick.AddListener(OnNext);

        upgradePanel.gameObject.SetActive(false);
        upgrade.gameObject.SetActive(false);
        upgrade.onClick.AddListener(OnToggleUpgrade);
        upgrade.transform.Find("Text").GetComponent<TMP_Text>()
            .text = GameManager.score.ToString();
        speedButton.onClick.AddListener(OnSpeedUp);
        rotationButton.onClick.AddListener(OnRotationUp);

        exit.onClick.AddListener(OnExit);

        victoryPanel.gameObject.SetActive(false);
        failedPanel.gameObject.SetActive(false);

        C.ec.showDockingUI += OnShowDockingUI;
        C.ec.interact += OnInteact;
        C.ec.tutorial += OnToggleInfo;
        C.ec.upgrade += OnToggleUpgrade;
        C.ec.closePanel += OnClosePanel;
        C.ec.cargoAdded += OnCargoAdded;
        C.ec.gameOver += OnGameOver;
    }

    private void OnInteact()
    {
        if (docking.gameObject.activeSelf)
        {
            OnDocking();
        }
        else if (sailAway.gameObject.activeSelf)
        {
            OnSailAway();
        }
    }

    private void OnShowDockingUI(bool show)
    {
        if (show)
        {
            docking.gameObject.SetActive(true);
            infoPanel.gameObject.SetActive(false);
        }
        else
        {
            docking.gameObject.SetActive(false);
            sailAway.gameObject.SetActive(false);
            victoryPanel.gameObject.SetActive(false);
            failedPanel.gameObject.SetActive(false);

        }
    }

    private void OnPre()
    {
        currentTutorial--;
        if (currentTutorial < 0)
        {
            currentTutorial = tutorialImages.Length - 1;
        }
        UpdateImage();
    }

    private void OnNext()
    {
        currentTutorial++;
        if (currentTutorial >= tutorialImages.Length)
        {
            currentTutorial = 0;
        }
        UpdateImage();
    }

    private void UpdateImage()
    {
        infoPanel.GetComponent<Image>().sprite = tutorialImages[currentTutorial];
    }

    private void OnDocking()
    {
        Debug.Log("OnDocking");
        docking.gameObject.SetActive(false);
        sailAway.gameObject.SetActive(true);
        upgrade.gameObject.SetActive(true);
        C.ec.docking.Invoke();
    }

    private void OnSailAway()
    {
        Debug.Log("OnSailAway");
        docking.gameObject.SetActive(false);
        sailAway.gameObject.SetActive(false);
        upgrade.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        C.ec.sailAway.Invoke();
    }

    public void OnToggleInfo()
    {
        infoPanel.gameObject.SetActive(!infoPanel.gameObject.activeSelf);
        preButton.gameObject.SetActive(!preButton.gameObject.activeSelf);
        nextButton.gameObject.SetActive(!nextButton.gameObject.activeSelf);
        if (!infoPanel.gameObject.activeSelf)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }
    
    public void OnToggleUpgrade()
    {
        upgradePanel.gameObject.SetActive(!upgradePanel.gameObject.activeSelf);
        if (upgradePanel.gameObject.activeSelf)
        {
            speedButton.enabled = GameManager.score >= GameManager.speedLevel;
            rotationButton.enabled = GameManager.score >= GameManager.rotationLevel;
            upgradePoint.text = GameManager.score.ToString();
            UpdateSpeedValue();
            UpdateRotationValue();
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    public void OnSpeedUp()
    {
        if (GameManager.score >= GameManager.speedLevel)
        {
            GameManager.score -= GameManager.speedLevel;
            upgrade.transform.Find("Text").GetComponent<TMP_Text>()
                .text = GameManager.score.ToString();
            GameManager.speedLevel++;
            speedButton.enabled = GameManager.score >= GameManager.speedLevel;
            upgradePoint.text = GameManager.score.ToString();
            UpdateSpeedValue();
            C.ec.upgradeSpeed.Invoke(GameManager.speedLevel);
        }
    }

    private void UpdateSpeedValue()
    {
        string text = "";
        for (int i = 0; i < GameManager.speedLevel; i++)
        {
            text += "+";
        }
        speedLevel.text = text;
        speedCost.text = $"-{GameManager.speedLevel}";
    }

    public void OnRotationUp()
    {
        if (GameManager.score >= GameManager.rotationLevel)
        {
            GameManager.score -= GameManager.rotationLevel;
            upgrade.transform.Find("Text").GetComponent<TMP_Text>()
                .text = GameManager.score.ToString();
            GameManager.rotationLevel++;
            rotationButton.enabled = GameManager.score >= GameManager.rotationLevel;
            upgradePoint.text = GameManager.score.ToString();
            UpdateRotationValue();
            C.ec.upgradeRotation.Invoke(GameManager.rotationLevel);
        }
    }

    private void UpdateRotationValue()
    {
        string text = "";
        for (int i = 0; i < GameManager.rotationLevel; i++)
        {
            text += "+";
        }
        rotationLevel.text = text;
        rotationCost.text = $"-{GameManager.rotationLevel}";
    }

    private void OnCargoAdded()
    {
        GameManager.score++;
        upgrade.transform.Find("Text").GetComponent<TMP_Text>()
            .text = GameManager.score.ToString();
    }

    private void OnClosePanel()
    {
        infoPanel.gameObject.SetActive(false);
        upgradePanel.gameObject.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
    }

    private void OnGameOver(bool victory)
    {
        if (victory)
        {
            victoryPanel.gameObject.SetActive(true);
            failedPanel.gameObject.SetActive(false);
            victoryPanel.transform.Find("Text").GetComponent<TMP_Text>()
                .text += GameManager.secret;
        }
        else
        {
            victoryPanel.gameObject.SetActive(false);
            failedPanel.gameObject.SetActive(true);
            failedPanel.transform.Find("Text").GetComponent<TMP_Text>()
                .text += GameManager.secret;
        }
    }

    public void OnExit()
    {
        Application.Quit();
    }

}
