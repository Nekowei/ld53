using Nekowei.YeahUtility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighthouseController : MonoBehaviour
{
    public Owner owner;
    public GameObject lighthouse;
    public GameObject lightRay;
    public GameObject lightQuad;
    public Light rayLight;
    public float rotateSpeed;

    public bool isSpecial = false;

    private float a;

    private void Start()
    {
        Color c = SOLoader.Get<OwnerColor>().GetColor(owner);
        lighthouse.GetComponent<Renderer>().material.color = c;
        lightQuad.GetComponent<Renderer>().material.color = c;
        rayLight.color = c;
        if (isSpecial)
        {
            lightRay.SetActive(false);
            C.ec.unload += OnUnload;
        }
        else
        {
            C.ec.raisingUp += OnRaisingUp;
        }
    }

    private void Update()
    {
        a += rotateSpeed * Time.deltaTime;
        if (a > 360)
        {
            a = 0;
        }
        lightRay.transform.rotation = Quaternion.Euler(0, a, 0);
    }

    private void OnRaisingUp(float height)
    {
        if (height > lightRay.transform.position.y)
        {
            lightRay.SetActive(false);
        }
    }

    private void OnUnload(Owner owner)
    {
        if (isSpecial && this.owner == owner)
        {
            if (GameManager.secret > 0)
            {
                lightRay.SetActive(true);
                C.ec.gameOver.Invoke(true);
            }
            else
            {
                C.ec.gameOver.Invoke(false);
            }
        }
    }

}
