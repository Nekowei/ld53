using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCenter
{

    public Action<Vector2> move { get; set; }
    public Action<Vector2> pointerMove { get; set; }
    public Action<float> zoom { get; set; }
    public Action<Vector2> drag { get; set; }
    public Action<Vector2> pointerUp { get; set; }
    public Action<Vector2> pointerDown { get; set; }

    public Action interact { get; set; }
    public Action closePanel { get; set; }
    public Action tutorial { get; set; }
    public Action upgrade { get; set; }

    public Action<int> upgradeSpeed { get; set; }
    public Action<int> upgradeRotation { get; set; }


    public Action<bool> showDockingUI { get; set; }
    public Action docking { get; set; }
    public Action sailAway { get; set; }

    public Action<GameObject> addCargo { get; set; }
    public Action cargoAdded { get; set; }
    public Action<Owner> unload { get; set; }

    public Action countdownOver { get; set; }
    public Action<float> raisingUp { get; set; }
    public Action<bool> gameOver { get; set; }
}
