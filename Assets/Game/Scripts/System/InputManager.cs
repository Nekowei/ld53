using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    private Vector3 lastMousePos;

    private void Update()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        if (x != 0 || y != 0)
        {
            C.ec.move.Invoke(new Vector2(x, y));
        }

        //if (UIPointerOverDetector.IsPointOverUI(out var g))
        //{
        //    ec.guiHover.action?.Invoke(Input.mousePosition, g);
        //}
        //else
        //{
            //ec.guiExit?.Invoke();
            HandleMouseInput();
        //}

        HandleKeyInput();
    }

    private void OnDestroy()
    {
        //GameManager.instance.ResetEC();
    }

    private void HandleMouseInput()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            C.ec.zoom.Invoke(Input.mouseScrollDelta.y);
        }

        bool button = false;
        if (Input.GetMouseButtonDown(0))
        {
            C.ec.pointerDown?.Invoke(Input.mousePosition);
            button = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            C.ec.pointerUp?.Invoke(Input.mousePosition);
            button = true;
        }
        else if (Input.GetMouseButton(0) && lastMousePos != Input.mousePosition)
        {
            C.ec.drag?.Invoke(Input.mousePosition);
            button = true;
        }

        if (Input.GetMouseButtonDown(1))
        {
            //ec.rightMouseDown?.Invoke(Input.mousePosition);
            button = true;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            //ec.rightMouseUp?.Invoke(Input.mousePosition);
            button = true;
        }

        if (!button)
        {
            //ec.pointerHover?.Invoke(Input.mousePosition);
        }
        if (Input.mousePosition != lastMousePos)
        {
            C.ec.pointerMove.Invoke(Input.mousePosition - lastMousePos);
        }

        lastMousePos = Input.mousePosition;
    }

    private void HandleKeyInput()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            C.ec.interact.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            C.ec.closePanel.Invoke();
        }

    }

}
